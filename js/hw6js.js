// forEach перебирает массив с помощью функции которой мы передаем аргументы item, itemindex, array.
// также можно передать только item.  в отличии от for\for of метод forEach явлеятеся более лаконичным/понятным




//1 способ//

function filterBy(incomingArray, typeAsFilterTool) {
    let filteredArray = incomingArray.filter(item => typeof item !== typeAsFilterTool);
    return filteredArray
}

let items = ['hello', 'world', 23, '23', null, undefined];
console.log(filterBy(items, "object"));


//2 способ//

// function filterBy(incomingArray, typeAsFilterTool) {
//     return incomingArray.reduce(function (filteredArray, currentItem) {
//         if (typeof currentItem !== typeAsFilterTool) {
//             filteredArray.push(currentItem);
//         }
//         return filteredArray
//     }, []);
//
// }
//
// console.log(filterBy(['hello', 'world', 23, '23', null, undefined], 'object'));




